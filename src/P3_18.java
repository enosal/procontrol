import java.util.Scanner;

/**
 * Input month and day (via prompt)
 * Output season
 *
 * Created by Eryka on 10/4/2015.
 */

public class P3_18 {
    public static void main(String[] args) {
        //initialize scanner for input, season variable to store season, getdate (to check for valid months)
        Scanner in = new Scanner(System.in);
        String season = "";
        boolean getdate = true;

        //Prompt & get input for Month
        System.out.print("Month: ");

        //only for integer inputs
        if (in.hasNextInt()) {
            int month = in.nextInt();

            //determine season by month
            if (month == 1 || month == 2 || month == 3) {
                season = "Winter";
            }
            else if (month == 4 || month == 5 || month == 6) {
                season = "Spring";
            }
            else if (month == 7 || month == 8 || month == 9) {
                season = "Summer";
            }
            else if (month == 10 || month == 11 || month == 12){
                season = "Fall";
            }
            else {
                System.out.println("Inappropriate month entered. Only enter numbers 1-12");
                getdate = false;
            }

            //Only asks for date if month is valid
            if (getdate) {
                //Prompt & get input for Day
                System.out.print("Day: ");

                //only for integer inputs
                if (in.hasNextInt()) {
                    int day = in.nextInt();

                    //determine season by day
                    if (month % 3 == 0 && day >= 21) {
                        if (season.equals("Winter"))
                            season = "Spring";
                        else if (season.equals("Spring"))
                            season = "Summer";
                        else if (season.equals("Summer"))
                            season = "Fall";
                        else
                            season = "Winter";
                    }
                    System.out.println("It is " + season);
                }

                //if day input is not an integer
                else {
                    System.out.println("Please only enter integers from 1-31");
                }

            }
        }

        //if month input is not an integer
        else{
            System.out.println("Please only enter integers from 1-12");
        }
    }
}