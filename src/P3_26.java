import java.util.Scanner;

/**
 * Input a positive integer (up to 3999)
 * Output value in roman numerals
 *
 * Created by Eryka on 10/4/2015.
 */
public class P3_26 {
    public static void main(String[] args) {
        //get input
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a number between 0 and 3999: ");
        int num = in.nextInt();

        //get positions
        int ones = num % 10;
        int tens = num % 100 - ones;
        int huns = num % 1000 - tens - ones;
        int thous = num % 10000 - huns - tens - ones;

//        System.out.println("ones: " + ones);
//        System.out.println("tens: " + tens);
//        System.out.println("huns: " + huns);
//        System.out.println("thous: " + thous);

        //initialize roman to store roman numeral
        String roman = "";

        //#### THOUSANDS SPOT #####//
        if (thous != 0) {
            //Add an M for every thousand
            for (int m=1000; m <= thous; m += 1000 ) {
                roman += "M";
            }
        }

        //### HUNDREDS SPOT ###//
        //900 is a special case
        if (huns == 900) {
            roman += "CM";
        }
        //500-800
        else if (huns >= 500 && huns < 900) {
            //Add a D for 500
            roman += "D";

            //Add a C for every 100 between 600-800 (inclusive)
            if (huns >= 600) {
                for (int c = 600; c <= huns; c += 100) {
                    roman += "C";
                }
            }
        }
        //400 is a special case
        else if (huns == 400) {
            roman += "CD";
        }
        //100-300
        else if (huns > 0 && huns < 400) {
            //Add a C for every 100 between 100-300 (inclusive)
            for (int c = 100; c <= huns; c += 100) {
                roman += "C";
            }
        }

        //#### TENS SPOT #####//
        //90 is a special case
        if (tens == 90) {
            roman += "XC";
        }
        //60-80
        else if (tens >= 50 && tens < 90) {
            //Add a L for 50
            roman += "L";

            //Add a X for every 10 between 60-80 (inclusive)
            if (tens >= 60) {
                for (int x = 60; x <= tens; x += 10) {
                    roman += "X";
                }
            }
        }
        //40 is a special case
        else if (tens == 40) {
            roman += "XL";
        }
        //10-30
        else if (tens > 0 && tens < 40) {
            //Add a X for every 10 between 10-30 (inclusive)
            for (int x = 10; x <= tens; x += 10) {
                roman += "X";
            }
        }




        //#### ONES SPOT #####//
        //9 is a special case
        if (ones == 9) {
            roman += "IX";
        }
        //5-8
        else if (ones >= 5 && ones < 9) {
            //Add a V for 5
            roman += "V";

            //Add an I for every 1 between 6-8 (inclusive)
            if (ones >= 6) {
                for (int i = 6; i <= ones; i += 1) {
                    roman += "I";
                }
            }
        }
        //4 is a special case
        else if (ones == 4) {
            roman += "IV";
        }
        //1-3
        else if (ones > 0 && ones < 4) {
            //Add an I for every 1 between 1-3 (inclusive)
            for (int i = 1; i <= ones; i += 1) {
                roman += "I";
            }
        }

        System.out.println(num + " in Roman Numerals is: " + roman);
    }
}
