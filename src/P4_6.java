import java.util.Scanner;

/**
 * Input integers (separated by space) and "quit"
 * Output the min integer
 * Created by Eryka on 10/4/2015.
 */
public class P4_6 {
    public static void main(String[] args) {
        //Initalize variables first, value, min
        boolean first = true;
        int value =  0;
        int min = 0;

        //Prompt & get input
        Scanner in = new Scanner(System.in);
        System.out.print("Enter numbers (separated by spaces) followed by \'quit\' to finish. Then press enter: ");

        //Get input at least once and continue to do so until a non-integer value (a string) is given
        do {
            //set the given int to value
            value =  in.nextInt();

            //if it's the first value given, set that equal to min and set first to false
            if (first) {
                min = value;
                first = false;
            }
            //if the given value is smaller than the min, update the min to that value
            else if (value < min) {
                min = value;
            }
        } while (in.hasNextInt());  //as long as an integer input is given, continue the loop

        System.out.println("Min is: " + min);


    }
}
