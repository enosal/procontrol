import java.util.Scanner;


/**
 * Input for income (prompt)
 * Output income tax
 * Created by Eryka on 10/4/2015.
 */
public class P3_21 {
    public static void main(String[] args) {
        //get input
        Scanner in = new Scanner(System.in);
        System.out.print("Enter income in dollars (no commas, no $): ");

        //initialize income, tax, givetax (to determine valid inputs) variables
        double income = in.nextDouble();
        double tax = 0;
        boolean givetax = true;

        //decision tree for income
        if (income >= 0 && income <= 50000 ){
            tax = 0.01 * income;
        }
        else if (income > 50000 && income < 75000) {
            tax = 0.02 * income;
        }
        else if (income >= 75000 && income < 100000) {
            tax = 0.03 * income;
        }
        else if (income >= 100000 && income < 250000) {
            tax = 0.04 * income;
        }
        else if (income >= 250000 && income < 500000) {
            tax = 0.05 * income;
        }
        else if (income >= 500000) {
            tax = 0.06 * income;
        }
        //wrong input type
        else {
            System.out.println("Improper income given");
            givetax = false;
        }

        //only print tax and income if a valid input was given
        if (givetax) {
            System.out.printf("Tax on $%.2f is: $%.2f", income, tax);
        }

    }
}
