import java.util.Scanner;

/**
 * Input three strings separated by a space
 * Output the lexographic ordering of the strings
 *
 * Created by Eryka on 10/3/2015.
 */
public class P3_16 {
    public static void main(String[] args) {
        //get input and assign to three string variables
        Scanner in = new Scanner(System.in);
        System.out.print("Enter three Strings: ");
        String string1 = in.next();
        String string2 = in.next();
        String string3 = in.next();

        //Initialize variables first, second, third for the orderings
        String first = ""; String second = ""; String third = "";

        /*Compare strings 1&2, 1&3, 2&3. Recall:
            compareX_Y < 0  string X comes before string Y
            compareX_Y > 0  string X comes after string Y
        */
        int compare1_2 = string1.compareTo(string2);
        int compare1_3 = string1.compareTo(string3);
        int compare2_3 = string2.compareTo(string3);

        //If string1 comes before strings 2 & 3
        if (compare1_2 <= 0 && compare1_3 <= 0) {
            //string1 is the first
            first = string1;

            //if string2 comes before string3
            if (compare2_3 <= 0) {
                //string2 is second; string3 is third
                second = string2;
                third = string3;
            }
            //else string3 comes before string2
            else {
                //string3 is second; string2 is third
                second = string3;
                third = string2;
            }
        }

        //If string2 comes before strings 1 & 3
        else if (compare1_2 >= 0 && compare2_3 <= 0) {
            //string2 is the first
            first = string2;

            //if string1 comes before string3
            if (compare1_3 <= 0) {
                //string1 is the second; string3 is the third
                second = string1;
                third = string3;
            }
            //else string3 comes before string1
            else {
                //string3 is the second; string1 is the third
                second = string3;
                third = string1;
            }
        }

        //if string3 comes before strings 1 & 2;
        else if (compare1_3 >= 0 && compare2_3 >= 0) {
            //string3 is the first
            first = string3;

            //if string 1 comes before string2
            if (compare1_2 <= 0) {
                //string1 is the second; string2 is the third
                second = string1;
                third = string2;
            }
            //else string2 is the second; string1 is the third
            else {
                second = string2;
                third = string1;
            }
        }
        System.out.println(first + "\n" + second + "\n" + third);

    }
}
