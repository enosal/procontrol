/**
 * Prints ASCII Art of a bunny saying "HI!"
 *
 * Created by Eryka on 9/28/2015.
 */


public class P1_10 {
    public static void main(String[] args) {
        System.out.println("           ___");
        System.out.println("  (\\ /)\t  /   \\");
        System.out.println("  (';')  < Hi! |");
        System.out.println("*\\(\")(\")  \\___/");

    }
}
