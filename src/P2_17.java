import java.util.Scanner;

/**
 * Input two military times
 * Output the difference between them in hours & minutes
 */


public class P2_17 {
    public static void main(String[] args) {
        //initalize first, second, difference in hours, difference in minutes
        int first = 0;
        int second = 0;
        int dif_hour = -1;
        int dif_min = -1;

        //get input
        Scanner in = new Scanner(System.in);
        System.out.print("Please Enter the first time (military time): ");
        if (in.hasNextInt()) {
           first = in.nextInt();
        }
        System.out.print("Please enter the second time (military time): ");
        if (in.hasNextInt()) {
            second = in.nextInt();
        }

        //get minutes
        int first_min = first % 100;
        int second_min = second % 100;

        //If the second time is larger than the first time
        if (second >= first) {
            /*if the minutes of the second time are larger than the minutes of the first time,
                Subtract firsttime from secondtime
                Int divide by 100 to get the hour
                Mod by 100 to get the minutes
            */
            if (second_min >= first_min) {
                int dif = second - first;
                dif_hour = dif / 100;
                dif_min = dif % 100;

            }

            /*If the minutes of the second time are smaller than the minutes of the first time,
                Add 60 to the second minutes, Subtract 100 from the second time (to remove an hour)
                Subtract first minute's from second's minutes to get the difference in minutes
                Subtract first hour's from second's hours to get the difference in hours
            */
            else {
                second_min += 60;
                int first_hour = first / 100;
                int second_hour = (second - 100) / 100;
                dif_min = second_min - first_min;
                dif_hour = second_hour - first_hour;
            }
        }

        /*If the first time is larger than the second time
            The general idea is to get the difference in times (subtract the second (smaller) time from the first (larger) time).
            Then subtract THAT difference from 2400
         */
        else{
            /* If the first minutes are larger than the second minutes
                Subtract the secondtime from the firsttime
                Int Divide the difference by 100. Then subtract from 23 (to account for the first min's being larger than the second minute's) to get the difference in hours
                Mod the difference by 100 to get the difference in minutes. Subtract from 60.
             */
            if (first_min >= second_min) {
                int dif = first - second;
                dif_hour = 23 - dif / 100;
                dif_min = 60 - dif % 100;

            }
            /* If the first minute's are smaller than the second's minutes
                Add 60 to the first minutes
                Subtract the second minutes from the first minutes. Subtract THAT difference from 60
                Subtract the second hour from the first hour. Subtract THAT difference from 24
            */
            else {
                first_min += 60;
                dif_min = 60 - (first_min - second_min);
                int first_hour = first / 100;
                int second_hour = second / 100;
                dif_hour = 24 - (first_hour - second_hour);
            }
        }
        System.out.println(dif_hour + " hours, " + dif_min + " minutes");
    }
}
