import java.util.Scanner;

/**
 * Input number
 * Print out all prime numbers
 * Created by Eryka on 10/4/2015.
 */
public class P4_18 {
    public static void main(String[] args) {
        //Prompt & get input
        Scanner in = new Scanner(System.in);
        System.out.print("Enter Number: ");
        int num =  in.nextInt();

        //index through all numbers leading up to the given number
        for (int i = 1; i <= num; i++) {

            //factors is the amount of numbers that divide into i
            int factors = 0;
            //index through all numbers leading up to i
            for (int divide_by = 1; divide_by <= i; divide_by++) {
                //if i can be divided by divide_by, then it is a factor of i
                if (i % divide_by == 0) {
                    factors += 1;
                }
            }
            //if i has two factors or less, those factors are itself and 1. It is prime. Print it as a prime number that leads up to our given #
            if (factors <= 2) {
                System.out.println(i);
            }
        }
    }
}
