import java.util.Scanner;

/**
 * Input suit (Q,D,S,H) for the first character followed by the card value (A, 2-10, J,Q,K).
 * Output the description of the card
 */


public class P3_14 {
    public static void main(String[] args) {
        //Get input
        Scanner in = new Scanner(System.in);
        System.out.print("Suit can be Q,D,S,H. \nValue can be A,2-10,J,Q,K. \nPlease enter a suit followed by value (i.e. SA for Ace of Spades): ");
        String card = in.next();

        //Initialize suit and value variables
        String suit = "";
        String value = "";

        //Parse the string into 2 parts - first is for Suit, second is for Card Value
        String first = card.substring(0,1);
        String second = card.substring(1);

        //first is for Suit
        if (first.equals("S")) {
            suit = "Spades";
        }
        else if (first.equals("D")) {
            suit = "Diamonds";
        }
        else if (first.equals("C")) {
            suit = "Clubs";
        }
        else if (first.equals("H")) {
            suit = "Hearts";
        }
        else {
            suit = "";
        }

        //second is for Card Value
        switch(second) {
            case "A": value = "Ace"; break;
            case "2": value = "Two"; break;
            case "3": value = "Three"; break;
            case "4": value = "Four"; break;
            case "5": value = "Five"; break;
            case "6": value = "Six"; break;
            case "7": value = "Seven"; break;
            case "8": value = "Eight"; break;
            case "9": value = "Nine"; break;
            case "10": value = "Ten"; break;
            case "J": value = "Jack"; break;
            case "Q": value = "Queen"; break;
            case "K": value = "King"; break;
            default: value = "";
        }

        //Make sure that there is both a valid suit and value
        if (suit.length() != 0 && value.length() !=0) {
            System.out.print(value + " of " + suit);
        }
        else{
            System.out.print("Error: Improper Input!");
        }

    }
}