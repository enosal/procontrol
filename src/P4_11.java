import java.util.Scanner;

/**
 * Input string
 * Output # of syllables
 * Created by Eryka on 10/4/2015.
 */
public class P4_11 {
    public static void main(String[] args) {
        //Prompt
        Scanner in = new Scanner(System.in);
        System.out.print("Enter word: ");

        //Initialize variables num_syl (# of syllables) and is_syl (is a syllable)
        int num_syl = 0;

        //Get only one word (that is a String)
        if (in.hasNext()) {
            //get input & length
            String word = in.next();
            int length = word.length();

            //Index through string
            for (int i = 0; i < length; i++) {
                //Get the letter at each position of the word, make it lowercase
                char letter = Character.toLowerCase(word.charAt(i));

                //if the letter is ae(e is not the last letter in the word)iouy
                if (letter == 'a' || (letter == 'e' & i != length-1) || letter == 'i' || letter == 'o' || letter == 'u' || letter == 'y') {
                    //increase the # of syllabus
                    num_syl +=1;

                    //if i is not the last position in the word
                    if (i != length-1) {
                            /*this is to skip the next letter -
                            if the next letter is a vowel, it counts as a syllable with the current letter
                            if the next letter is a constant, we wouldn't count it as a syllable anyways
                             */
                            i++;
                        }
                }
            }
            //if 0 syllable found using this method, set num_syl to 1.
            if (num_syl == 0) {
                num_syl = 1;
            }
            System.out.println("Number of syllables for " + word + " is: " + num_syl);
        }
    }
}
